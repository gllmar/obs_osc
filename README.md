# obs_osc
obs osc (open sound control) remote 

## midi2osc4obs

For obs 28 and up, needs osc-for-obs

### OSC-for-OBS

Patch index.js with SetCurrentSceneTransitionDuration around line 2100
```
        //Set SetCurrentSceneTransitionDuration
    else if(msg[0].includes('/SetCurrentSceneTransitionDuration')){
        console.log(`OSC IN: ${msg}`)
        logEverywhere(`OSC IN: ${msg}`)
        var msgArray = msg[0].split("/")
        msgArray.shift()
        console.log("Messge array: " + msgArray)
        logEverywhere("Messge array: " + msgArray)
        return obs.call("GetCurrentProgramScene").then(data => {
        obs.call("SetCurrentSceneTransitionDuration", {
            'transitionDuration': msg[1]
        }).catch(() => {
            logEverywhere("ERROR: Invalid SetCurrentSceneTransitionDuration Type Syntax. See Help > API")
        })
    })
    }
```

if VST cannot be run in terminal

```
xattr -rd com.apple.quarantine [drag vst]
```

## midi2obsctl

For OBS 27 and less
## dependencies

### brew

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```


### git
```
brew install git 
```


### OBS

```
brew cask install obs
```

### obs-websocket 

[https://github.com/Palakis/obs-websocket/releases](https://github.com/Palakis/obs-websocket/releases)

### python pip

```
brew install pip
ou
sudo easy_install pip
```

### obs-websocket-py

```
pip3 install obs-websocket-py
```

### cython

```
brew install cython
pip install cython
```

### liblo

```
brew install liblo
pip install pyliblo
```


## install this repo

git clone 
