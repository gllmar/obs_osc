#/bin/bash
SCRIPTPATH=$(dirname $(realpath -s $0))
echo $SCRIPTPATH
echo $DISPLAY
sleep 1
xrandr --output LVDS1 --primary --mode 1366x768 --pos 0x0 --rotate normal --output DP1 --off --output DP2 --off --output DP3 --off --output HDMI1 --mode 1280x720 --pos 1366x0 --rotate normal --output HDMI2 --off --output HDMI3 --off --output VGA1 --off --output VIRTUAL1 --off
sleep 1
systemctl --user restart obs-ctl-obs.service
sleep 1
systemctl --user restart obs-ctl-osc-server.service
