#!/usr/bin/env python

# import osc related libs
from __future__ import print_function
from obswebsocket import obsws, requests
from liblo import * 

# import timing
import time
import threading

import time
import logging
logging.basicConfig(level=logging.INFO)


host = "localhost"
port = 4444
password = "secret"


# globals

global_absolute_scene = "NULL"
global_midi_note = "NULL"
global_midi_note_old = "NULL"
global_midi_time = "NULL"
global_midi_time_old = "NULL"

# raw_input renamed to input in python3
try:
	input = raw_input
except NameError:
	pass


class OSC_server(ServerThread):
	def __init__(self):
		ServerThread.__init__(self, 1234)

	@make_method('/scene', 's')
	def scene(self, path, args):
		global global_absolute_scene
		global_absolute_scene, = args
		print("%s" " %s " % (path, global_absolute_scene))

	@make_method('/midi/note', 'ff')
	def midinote(self, path, args):
		global global_midi_time
		global global_midi_note
		global_midi_note, global_midi_time = args
		print("%s" " %s " " %s " % (path, global_midi_note, global_midi_time))

	@make_method('/midi/time', 'f')
	def miditime(self, path, args):
		global global_midi_time
		global_midi_time, = args
		print("%s" " %s " % (path, global_midi_time))	

	@make_method('/midi/scene', 'f')
	def midiscene(self, path, args):
		global global_midi_note
		global_midi_note, = args
		print("%s" " %s " % (path, global_midi_note))


class websocket_tread( threading.Thread ):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.name=name
		self.scene="NULL"
		self.host = "localhost"
		self.port = 4444
		self.password = "secret"

		self.ws = obsws(host, port, password)
		self.ws.connect()
		try:
			self.scenes = self.ws.call(requests.GetSceneList())
			print(self.scenes)
		except:
			print("error  thread")

	def run(self):
		global global_absolute_scene
		global global_midi_note
		global global_midi_note_old
		global global_midi_time
		global global_midi_time_old
		
		while True:
			# si la scene recu est diff de celle actuelle et que la scene recu est dans la liste des scenes
			if global_absolute_scene != self.scene:
				self.scene=global_absolute_scene
				self.ws.call(requests.SetCurrentScene(self.scene))
			if global_midi_time_old != global_midi_time:
				global_midi_time_old=global_midi_time
				try:
					self.ws.call(requests.SetTransitionDuration(int(global_midi_time)))
				except:
					pass
			if global_midi_note_old != global_midi_note:
                # changer vers la nouvelle scene 
				global_midi_note_old=global_midi_note
				try:
					self.scene=str(int(global_midi_note))
					self.ws.call(requests.SetCurrentScene(self.scene))
				except:
					print("error midi note")
			time.sleep(0.1)

try:
	server = OSC_server()
except ServerError as err:
	print(err)
	sys.exit()
	
try:
	obs_websocket = websocket_tread(1, 1)
except:
	print("error socket  thread")

server.start()
obs_websocket.start()


input("osc on 1234, websock on 4444 \n press enter to quit...\n")

obs_websocket.ws.disconnect()
