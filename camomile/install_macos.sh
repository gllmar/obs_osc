#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo "=> cp -r $SCRIPTPATH/builds/MacOS/*.vst3 ~/Library/Audio/Plug-Ins/VST"
cp -r $SCRIPTPATH/builds/MacOS/*.vst3 ~/Library/Audio/Plug-Ins/VST