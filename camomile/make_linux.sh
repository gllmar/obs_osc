#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

CAMO_BIN="$SCRIPTPATH/camomile-bin/camomile-linux/camomile" 

$CAMO_BIN -d "$SCRIPTPATH/src" -o "$SCRIPTPATH/builds/linux"

#/home/artificiel/src/obs_osc/camomille/camomile-bin/camomille-linux/camomille